function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

local bb = {}
bb.realtext = ""
bb.italic = love.graphics.newFont("gui/font/KaushanScript.otf", 16*fontsize)
bb.aftercall = {}

function bb.clean(itext)
  itext.colormask = {}
  itext.fontmask = {}
end

function bb.make(itext)
  --clean
  bb.clean(itext)
  bb.realtext = itext:GetText()
  
  --now make
  local lines = itext:GetLines()
  local i = 1
  itext.colormask[1] = {255,255,255,255}
  for ir, line in ipairs(lines) do
    local st = nil
    if line:starts("#") then
      st = string.find(line, "_")
      if st ~= nil then
        itext:SetLine(ir, line:sub(st+1))
      end
    end
    if line:starts("#task_") then
      itext.colormask[i] = {255,75,250,255}
      itext.fontmask[i] = bb.italic
    
    -- Dialog colors
    elseif line:starts("#1_") then
      itext.colormask[i] = {255,50,0,255}
    elseif line:starts("#2_") then
      itext.colormask[i] = {100,255,0,255}
    elseif line:starts("#3_") then
      itext.colormask[i] = {0,150,250,255}
    elseif line:starts("#4_") then
      itext.colormask[i] = {255,102,0,255}
    elseif line:starts("#5_") then
      itext.colormask[i] = {243,174,174,255}
    elseif line:starts("#6_") then
      itext.colormask[i] = {180,190,255,255}
    elseif line:starts("#7_") then
      itext.colormask[i] = {255,220,30,255}
    elseif line:starts("#8_") then
      itext.colormask[i] = {233,173,122,255}
    end
    
    -- Battle
    if line:starts("#spawn:") then
      local name = line:sub(8, st-1)
      name = name:gsub("%-", "_")
      table.insert(bb.aftercall, function() battle.spawn(name) end)
    end
    
    if line == "" then
      i = i + 1
      itext.colormask[i] = {255,255,255,255}
    end
  end
end

return bb