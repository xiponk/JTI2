local effect = {}
local textspeed = 0.02
local textwait = 3

effect.Timer = Timer.new()
effect.before = {}
effect.after = {}
effect.font = love.graphics.newFont("gui/font/Hobo.ttf", 40)
effect.fontmini = love.graphics.newFont("gui/font/Hobo.ttf", 25)

function effect.addBeforeNext(name, ...)
  table.insert(effect.before, {name, {...}})
end
  
function effect.addAfterLast(name, ...)
  if name == "textFadeInParagraphs" then
    scene.list.slideshow.itext.colormask = {{255,255,255,0}}
  end
  table.insert(effect.after, {name, {...}})
end

function effect.cleanup()
  effect.before = {}
  effect.after = {}
  Effect.Timer:clear()
end

-- Every effect runs this function when finalizing the effect.
function effect.runAfterQueue()
  if effect.after[1] == nil then return end
  local name = effect.after[1][1]
  local args = effect.after[1][2]
  effect[name](unpack(args))
  
  table.remove(effect.after, 1)
end

function effect.runBeforeQueue()
  if effect.before[1] == nil then
    local x = scene.list.slideshow
    x.goNext()
    if x.imagebutton.OnClick ~= false then
      x.imagebutton.OnClick = x.goNext
    end-- Restore next button functionality
    return
  end
  local name = effect.before[1][1]
  local args = effect.before[1][2]
  effect[name](unpack(args))
  
  table.remove(effect.before, 1)
end

function effect.draw()
  function drawtext(ik)
    if ik == nil then return end
    love.graphics.setColor(unpack(ik[2]))
    love.graphics.setFont(ik[3])
    love.graphics.print(ik[1], ik[4], ik[5])
    love.graphics.setColor(255,255,255,255)
  end
  
  drawtext(invoked_text)
  drawtext(invoked_text_mini)
end
----------------------------------------------------------------------
                ---- Actual effects start here ----
----------------------------------------------------------------------

local itextpi = 1
function effect.makeColormaskInvisible(itext)
  local lines = itext:GetLines()
  local l = 1
  for i=1, #lines do
    local str = lines[i]
    if str and str ~= "" then
      if type(itext.colormask[l]) == "table" then itext.colormask[l][4] = 0
      else itext.colormask[l] = {255,255,255,0} end
      l = l+1
    end
  end
  itext.colormask[l] = {255,255,255,255}
end

function effect.textFadeInParagraphs(time)
  local itext = scene.list.slideshow.itext  
  if time == nil then time = 4 end
  function run()
    if type(itext.colormask[itextpi]) ~= "table" then
      effect.runAfterQueue()
      return
    end
    local str = itext:GetLines()[itextpi*2-1]
    if not str then return end
    local ct = #str * textspeed
    
    local r, g, b = 255, 255, 255
    if itext.colormask[itextpi] then
      r, g, b = unpack(itext.colormask[itextpi])
    end
    
    effect.Timer:tween(time, itext.colormask[itextpi], {r, g, b, 255}, "linear")
    effect.Timer:after(ct+textwait, run)
    
    itextpi = itextpi + 1
  end
  
  effect.makeColormaskInvisible(itext)
  itextpi = 1
  run()
end

function effect.textFadeOutParagraphs(sec)
  local itext = scene.list.slideshow.itext
  
  local forth = {}
  for _, mask in ipairs(itext.colormask) do
    table.insert(forth, mask[4])
  end
  table.remove(forth) -- The last element of a colormask is usually a flag
  
  local m = math.max(unpack(forth))
  sec = sec*m/255
  for _, mask in ipairs(itext.colormask) do
    effect.Timer:tween(sec, mask, {[4] = 0}, "linear")
  end
  effect.Timer:after(sec, effect.runBeforeQueue)
end 

function effect.wait(sec)
  effect.Timer:after(sec, effect.runAfterQueue)
end

function effect.blackFadeOut(sec)
  local scn = scene.list.slideshow
  scene.color = {0, 0, 0, 255}
  effect.Timer:tween(sec or 3, scene.color, {0, 0, 0, 0}, "cubic")
  scn.imagebutton.OnClick = scn.goNext
end

function effect.blackFadeIn(sec)
  scene.list.slideshow.imagebutton.OnClick = false
  scene.color = {0, 0, 0, 0}
  effect.Timer:tween(sec or 2, scene.color, {0, 0, 0, 255}, "linear", effect.runBeforeQueue)
end

function effect.invokeText(stra, strb, x, y, z)
  local r, g, b = 255, 255, 255 -- invoked text color
  local i, j, k = 255, 30, 30 -- invoked text mini color
  
  if x == nil then x = 2 end
  if y == nil then y = 4 end
  if z == nil then z = x end
  local width = effect.font:getWidth(stra)
  local hight =  effect.font:getHeight()
  invoked_text =      {stra, {r,g,b,0}, effect.font, 960/2 - width/2, 540/2 - hight}
  
  width = effect.fontmini:getWidth(strb)
  invoked_text_mini = {strb, {i,j,k,0}, effect.fontmini, 960/2 - width/2, 540/2 - effect.fontmini:getHeight() + hight/2}
  
  function run()
    effect.Timer:tween(z, invoked_text[2], {r,g,b,0}, "linear", effect.runAfterQueue)
    effect.Timer:tween(z, invoked_text_mini[2], {i,j,k,0}, "linear")
  end
  
  effect.Timer:tween(x, invoked_text[2], {r,g,b,255}, "linear",
    function() effect.Timer:after(y, run) end)
  effect.Timer:tween(x, invoked_text_mini[2], {i,j,k,255}, "linear")
end

function effect.fadeToSong(path)
  music.isFading = true
  Timer.tween(3, music, {volume=0}, "linear", function()
      music.isFading = false
      quest.playSong(path)
      end)
end

return effect