                 Guide to Izildia

I, the humbe author of this work, Edgar Thales, will introduce you the basic knowloadge you need when traveling through the land of Izildia. As an adventurer it's mandatory you know the basics of the place of you want to properly communicate and understand it's creatures and people.

Izildia is a Land of about 150km wide. Surrounding Izildia at the east there are a number of mountain ranges, the highest mountain known, the world's mountain, is there. To the north you will find the Farline sea, to the south a desert, and at the west an unpenetrable forest covered in mistery, some rumors say that eleves inhabit there.

There are four major cities on the land, starting with the capital, Reco's Town, Northdoor, Skycity and Shalk.

Despite what the name implies, Reco's Town is the greatest city in all Izildia, thanks to it's market and the propsperity of the last hundred years Reco's Town has grown to an incredible rate with a population avobe the twenty thousend. Reco's Town is found at the center of the land, if you plan to visit there it's recommendable to wear good clothes, you don't want to be confused with a slave.

Norhdoor has also grown greatly in the last century, now becoming the largest city and the second most populated. Contrary to Reco, Northdoor has abolished slavery and the queen makes sure to enforce the self-proclamed "human-rights". However as much good this city may seem don't even think to come near Norhdoor if you are not completly human.

About Skycity and Shalk I wasn't able to get much information. You can only enter Skycity with a visa from Northdoor, and it is pretty hard to get one. To enter Shalk is much more easier, but who would want to go inside a city full of thiefs, murderers and degenerated criminals.

Besides the main four cities, there are a lot of other nice places to visit when traveling arround Izildia, the lake Valeria, the Dripping Pantie Inn, or the Western Castle are some of them.

If gastronomy is what you're looking for, the Reco's Market is surrounded with good and prety costly restaurants that will make you lick your fingers.

If your mission is knowladge Skycity is famous for it's great library, great technology and a place called university.

The most common race in Izilida are humans, but don't expect only humans in this land is more than a third of its population are spicies that only recently stoped being considered monsters. Such spices include orcs, goblins, succubus and imps. Deamons, minotaurs and other monsters can also be found pretty frecuently so make sure to be ready to take them in combat before you venture to go outside a main road.

There is also a special group of spicies found in Izildia, they are very rare and extremely precious if you're gona sell them. It includes fairyies, elfs and the terrifying dragons.

As you can see, going outside the main roads it's very dangerous for the unprepared, if you want to train and improve you combat skills consider the Shalk coliseum, known for the generous prizes for the winners.

And with this I give for completed my work, thanks for purchasing this traveling guide. If you hapen to be a beatiful elf you may find me frequently at the Dripping Pantie Inn, I hope we can meet there soon.   